import datetime
from typing import Any
from django.db import models
from django.utils.html import format_html
from django.core.exceptions import ValidationError

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    created_date = models.DateField(auto_now_add=True)
    closed = models.BooleanField(default=False)

    def __str__(self):
        return self.name + " - " + self.description
    
    def colored_id(self):
        if self.id % 2:
            color = 'green'
        else:
            color = 'red'
        return format_html(
            '<span style="background-color: {};">{} - {}</span>',
            color,
            self.id,
            self.name
        )
    
    colored_id.allow_tags = True

class TaskForm(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    created_date = models.DateField(auto_now_add=True)
    closed = models.BooleanField(default=False)

    def __str__(self):
        return self.name + " - " + self.description

    def clean(self):
        if self.name == self.description:
            raise ValidationError('Name and description must be different')
        if self.created_date > datetime.date.today():
            raise ValidationError('Created date must be in the past')
        if self.closed and self.created_date == datetime.date.today():
            raise ValidationError('Closed tasks cannot be created today')
        if self.closed and self.created_date > datetime.date.today():
            raise ValidationError('Closed tasks cannot be created in the future')
    
    def save(self, *args, **kwargs):
        self.full_clean()
        super(Task, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.full_clean()
        super(Task, self).delete(*args, **kwargs)