import datetime
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Task

# Create your views here.
def index(request):
    return HttpResponse("<h1> Hello, world.</h1> <br> You're at the polls index.")

def home(request, name):
    return HttpResponse("<h1> Hello,"+ name +".</h1> <br> You're at the "+ name +"'s home.")

def list_tasks(request):
    tasks = Task.objects.all()
    return render(request, 'list_tasks.html', {'tasks': tasks})

def infos_task(request, id):
    # task = Task.get_objects.get(id=id)
    task = get_object_or_404(Task, pk=id)
    return render(request, 'infos_task.html', {'task': task})

def create_task(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        task = Task(name=name, description=description)
        task.save()
        return HttpResponseRedirect(reverse('polls:list_tasks'))
    else:
        return render(request, 'create_task.html')

def delete_task(request, id):
    task = get_object_or_404(Task, pk=id)
    task.delete()
    return HttpResponseRedirect(reverse('polls:list_tasks'))