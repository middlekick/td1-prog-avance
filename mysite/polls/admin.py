from django.contrib import admin
from django import forms
from .models import Task

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'created_date', 'closed', 'colored_id')

# Register your models here.
# admin.site.register(Task)